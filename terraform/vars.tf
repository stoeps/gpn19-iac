variable "vsphere_server" {
  default = "khnum.example.com"
}

variable "vsphere_user" {
  default = "cstoettner@example.com"
}

variable "vsphere_password" {
  description = "vsphere server password for the environment"
  default     = ""
}

variable "vsphere_datacenter" {
  default = "HVIE"
}

variable "vsphere_cluster" {
  default = "HVIE PWR HOSTS"
}

variable "vsphere_datastore" {
  default = "devops-01_sas_7.2k_raid10"
}

variable "vsphere_network" {
  default = "vm-net-devops"
}

variable "vsphere_resource_pool" {
  default = "rp_hvie_devops"
}

variable "pana_devops_folder" {
  default = "devops"
}

variable "vsphere_domain" {
  default = "devops.example.com"
}

variable "vsphere_dns_servers" {
  type    = list(string)
  default = ["10.10.85.5"]
}

variable "admin_user" {
  default = "root"
}

variable "admin_password" {
  default = "password"
}

variable "template" {
  default = "centos-76-base"
}

variable "ssh-pub-key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDzlkLL0RzRLmNCP/Hw/7oX5l5HlIY+2wSQpEPLafOG/ocDubvmuxX1GgOTXPMNcIJiCF9RFnfpxcLIL4PkEjgQ7q/N91W3Z+bXztUJj/pNpodM68yqqRgeDJF+kLPa0dxM3At3I5dljs4uV543jTzo0pceSr6430X/rPnREMm9NtxL4I41U4JHMHtIyWYvJxyYuSKY7t6ioGNwTccI45lHLhMw3NVDaQ8F1m9Xz2t1UbGNG9gQfRP6Vix2UnA6U46sjua0EiRonYjy0aibjT8NXbf9SZNfonJ/NofGb7GBeK9AXn/DlCXs83swRB1zgCi/g9QfsNWu4fyFoJC6Eu1Si80kE42RLE6CHsBxf614LDuzh7iiu1yhzt8elPCsSACwsKvZq9LMHG1kUOZLm25eCN3tt5Hxs/cteL+AfZ0NUQ+KwffSafkO7W30W6yoKBPqq/oyZnZLS+1/bXQKiQGzChtypXiM137XnH07coijh9evNSrpMU+RXjDsGX05tb8e6F+LTaKqXaiTt3kiFDLGbyoNCymlY1vDaYykkF0bTXkKujIlm6YcJ6Jdpf9gUSUANHQGqRDBy1FcbO70f2HQylcSN6R7QJ26RmhDzYODvVyROIkoeDIgdwFIlUcJqoEITl5sTaGchYoJMeN/coe6CNVB+FDJvz98f3PGqyNsgQ== christoph.stoettner@stoeps.de"
}
