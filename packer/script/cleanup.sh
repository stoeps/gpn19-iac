#!/bin/bash -eux

echo "==> Cleaning up temporary network addresses"

DISK_USAGE_BEFORE_CLEANUP=$(df -h)

# Other locales will be removed from the VM
KEEP_LANGUAGE="en"
KEEP_LOCALE="en_US"
echo "==> Remove unused man page locales"
pushd /usr/share/man
if [ $(ls | wc -w) -gt 16 ]; then
  mkdir ../tmp_dir
  mv man* $KEEP_LANGUAGE $SECONDARY_LANGUAGE ../tmp_dir
  rm -rf *
  mv ../tmp_dir/* .
  rm -rf ../tmp_dir
  sync
fi
popd

# echo "==> Remove packages needed for building guest tools"
# yum -y remove gcc cpp libmpc mpfr kernel-devel kernel-headers

echo "==> Clean up yum cache of metadata and packages to save space"
yum -y --enablerepo='*' clean all

echo "==> Clear core files"
rm -f /core*

echo "==> Removing temporary files used to build box"
rm -rf /tmp/*

echo "==> Rebuild RPM DB"
rpmdb --rebuilddb
rm -f /var/lib/rpm/__db*

# Enable NIC
echo "==> Enable NIC during boot"
sed -i -e 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-ens33

# Delete SSH Host Keys (rebuild after restart)
echo "==> Remove SSH Host Keys"
rm -f /etc/ssh/ssh_host_*

# Delete machine-id (see: https://www.thegeekdiary.com/centos-rhel-7-how-to-change-the-machine-id/)
rm -f /etc/machine-id

# Block until the empty file has been removed, otherwise, Packer
# will try to kill the box while the disk is still full and that's bad
sync

echo "==> Disk usage before cleanup"
echo ${DISK_USAGE_BEFORE_CLEANUP}

echo "==> Disk usage after cleanup"
df -h
