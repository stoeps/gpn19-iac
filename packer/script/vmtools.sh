#!/usr/bin/env bash
yum -y install open-vm-tools

# vSphere provisioning needs perl
echo '==> Install perl'
yum -y install perl

echo '==> Restarting open-vm-tools'
systemctl restart vmtoolsd