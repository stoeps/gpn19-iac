#!/usr/bin/env bash

echo "==> Updating Kernel to newest version"
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
yum -y install yum-plugin-fastestmirror
yum -y --enablerepo=elrepo-kernel install kernel-ml
sed -i 's/GRUB_DEFAULT=saved/GRUB_DEFAULT=1/g' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg
